### Автоответчик на отзывы в ВБ ###

## Текущая версия программы 1.2 ##

## Первичная настройка программы ##

Находим в телеграм бота [@wb_fb_autoresponder_bot](https://t.me/wb_fb_autoresponder_bot)

Нажимаем **Старт** чтобы бот имел права отправить Вам уведомления в случае ошибки (при разлогиневании в ВБ) и данные по отзывам где оценка не равна 5. Бот пришлет ваш **telegram id**, данный id нам понадобятся дальше.... 

Так нужно сделать со **всех** telegram аккаунтов по которым хотите получать уведомления.

![image](/instructions/img/bot_msg.JPG)

Скачеваем архив с программой по ссылке с [Gitlab](https://gitlab.com/hafizov.azat/Feedback_Autoresponder_win/-/archive/main/Feedback_Autoresponder_win-main.zip)

Открываем скачанный архив и разархивируем папку в архиве со скаченным файлом в любое удобную директорию где в последующем его не удалят по ошибке, например в корень диска **С:/**

![image](/instructions/img/arhive_load.JPG)

Открываем эксель документ **Ответы.xlsx** в разархивированной папке с программой и правим следующие данные:

Правим данные по **telegram id** которые мы ранее получали уведомлении, забиваем их в странице **“Телеграм”**. Так же можно вписать свой **bot api** в ячейке **B2** если Вы хотите получать уведомления в другом telegram боте

![image](/instructions/img/tg_notif_set.JPG)

Так же нужно поправить в странице **“Рекомендации”** вариацию рекомендаций, если на один артикул несколько рекомендаций, программа будет выбирать рандомную вариацию

![image](/instructions/img/recommend.JPG)

Так же можно поправить страницу **“Ответ”**. В ответах допускаются следующие константы:

>> **$brand$** - подставиться бренд по артикулу с ВБ у текущего отзыва

>> **$articul$** - подставиться артикул с ВБ у текущего отзыва

>> **$seller$** - подставиться продавец у текущего артикула с ВБ

В столбе рекомендации допускаются следующие константы

>> **$orig_name$** - подставиться Название продукта (**первый столб**) со страницы **“Рекомендации”** по артикулу

>> **$recommend_name$** - подставиться имя рекомендуемого товара (**третий столб**) со страницы **“Рекомендации”** по артикулу

>> **$recommend_articul$** - подставиться артикул рекомендуемого товара (**четвертый столб**) со страницы **“Рекомендации”** по артикулу

![image](/instructions/img/answers.JPG)

Сохраняем файл и запускаем программу для авторизации, запустив **autorisathion.exe**

![image](/instructions/img/autorisation.JPG)

Придет уведомление в телеграм (если вы вписали правильно telegram idшники)

Далее установим службу запустив **setup.exe**

![image](/instructions/img/service_install.JPG)

В системе создается служба **“WB автоответчик на отзывы”**. Проверяем что служба успешно запустилась нажав комбинацию клавиш **win** + **R** в появившемся окне пишем **services.msc** и нажимаем **enter**

![image](/instructions/img/service_view.JPG)

**Процесс первичной настройки пройден!**

## Авторизация сессии в ВБ

При разлогинивании сессии в ВБ проходим авторизацию запустив **autorisathion.exe**

Проходим стадию аутентификации как это делали при первичном запуске, проходим в папку с программой, запускаем **autorisathion.exe** и проходим авторизацию

![image](/instructions/img/autorisation.JPG)

**Авторизация пройдена!**

## Обновление программы

Нажимаем комбинацию клавиш **win** + **R** в появившемся окне пишем **services.msc** и нажимаем **enter**, открывается окно _“Службы”_. Останавливаем службу **“WB автоответчик на отзывы”** нажав **“Остановить”**:

![image](/instructions/img/service_stop.JPG)

Скачеваем архив с программой по [ссылке](https://gitlab.com/hafizov.azat/Feedback_Autoresponder_win/-/archive/main/Feedback_Autoresponder_win-main.zip)

Открываем скачанный архив и заменяем 3 файла (**autoresponder.exe**, **autorisathion.exe** и **setup.exe**) в папке с программой файлами с архива:

![image](/instructions/img/update_download.JPG)

Обратно запускаем службу нажав **“Запустить”**

![image](/instructions/img/service_start.JPG)

**Программа обновлена!**
